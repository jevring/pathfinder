package net.jevring.pathfinder;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class Obstacle extends Polygon {
	public static final int WIDTH = 50;
	public static final int HEIGHT = 50;
	
	public Obstacle(Point middle) {
		addPoint(middle.x - (WIDTH / 2), middle.y - (HEIGHT / 2));
		addPoint(middle.x + (WIDTH / 2), middle.y - (HEIGHT / 2));
		addPoint(middle.x + (WIDTH / 2), middle.y + (HEIGHT / 2));
		addPoint(middle.x - (WIDTH / 2), middle.y + (HEIGHT / 2));
	}
	
	public List<Point> getCorners() {
		List<Point> corners = new ArrayList<Point>();
		for (int i = 0; i < npoints; i++) {
			corners.add(new Point(xpoints[i], ypoints[i]));
		}
		return corners;
	}

	public void merge(Obstacle obstacle) {
		/*
		Find the point that is inside.
		Find the intersection of the current thing and the point and create those new points according to the thing.
		 */
		for (Point point : obstacle.getCorners()) {
			if (this.contains(point)) {
				Line lineToPoint;
				Line lineFromPoint;
				/*
				Find out which lines of THIS obstacle intersects the lines going to this point.
				Actually, since we're dealing with arbitrary shapes, we don't necessarily have two lines that intersect.
				A new obstacle can hide MANY lines
				 */
				/*
				for (Line line : this.getLines()) {
					Point lineToIntersection = line.intersection(lineToPoint);
					Point lineFromIntersection = line.intersection(lineFromPoint);
					if (lineToIntersection == null) {
						// 
					}
				}
				*/
			}
		}

	}
	
	private static final class Line {
		
	}
}
