package net.jevring.pathfinder;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.Executors;

/**
 * @author markus@jevring.net
 */
public class Main {
	public static void main(String[] args) {
		Pathfinder pathfinder = new Pathfinder();
		
		// create the window for the application
		JFrame frame = new JFrame("Pathfinder");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setPreferredSize(new Dimension(1000, 700));
		frame.add(pathfinder);
		frame.pack();
		frame.setLocation(500, 200);
		frame.setVisible(true);

		Executors.newCachedThreadPool().execute(pathfinder);
	}
}
