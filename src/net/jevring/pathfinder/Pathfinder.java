package net.jevring.pathfinder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import java.util.Queue;

/**
 * @author markus@jevring.net
 */
public class Pathfinder extends JPanel implements Runnable {
	private final List<Obstacle> obstacles = new ArrayList<Obstacle>();
	private final Queue<Point> waypoints = new LinkedList<Point>();
	private volatile Point currentWaypoint = null;
	private volatile Obstacle currentObstacle = null;
	private volatile Point mouseLocation;
	private volatile Point thingLocation;
	private volatile Point movementAimTarget;
	private volatile int speed = 3;
	private volatile int delay = 100;
	private volatile boolean paused = false;
	
	public Pathfinder() {
		setBackground(Color.WHITE);
		setForeground(Color.BLACK);
		addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				mouseLocation = e.getPoint();
				repaint();
			}
		});
		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isShiftDown()) {
					addWaypoint(e.getPoint());
				} else {
					dropObstacle(e.getPoint());
				}
			}
		});
		addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.getWheelRotation() < 0) {
					delay += 5;
				} else {
					delay -= 5;
				}
				delay = Math.max(10, delay);

			}
		});
		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0), "Pause");
		getActionMap().put("Pause", new AbstractAction("Pause") {
			@Override
			public void actionPerformed(ActionEvent e) {
				paused = !paused;
			}
		});
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// todo: why am I not getting any key events?
				// because KeyListeners don't work. Use KeyBindings instead, as above
				//System.out.println("e = " + e);
				if (e.getKeyCode() == KeyEvent.VK_LEFT) {
					delay += 5;
				} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
					delay -= 5;
				} else if (e.getKeyCode() == KeyEvent.VK_UP) {
					speed++;
				} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					speed--;
				}
				repaint();
			}
		});
	}

	private void addWaypoint(Point point) {
		waypoints.add(point);
	}

	/**
	 * Places an obstacle in the way that the thing has to navigate around.
	 * @param point the point where to place the obstacle
	 */
	private void dropObstacle(Point point) {
		/*
		Check if we're part of something else, and if so, merge with it
		 */

		Obstacle obstacle = new Obstacle(point);
		List<Point> corners = obstacle.getCorners();
		for (Obstacle existingObstacles : obstacles) {
			for (Point corner : corners) {
				if (existingObstacles.contains(corner)) {
					System.out.println("Existing obstacle " + existingObstacles + " contains corner " + corner);
					// todo: make a list with things that we should merge
					existingObstacles.merge(obstacle);
					break;
				}
			}
		}
		obstacles.add(obstacle);
		
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g); // this clears the background
		g.setColor(Color.BLACK);
		g.drawString("Speed: " + speed, 10, 20);
		g.drawString("Delay: " + delay, 10, 40);
		if (paused) {
			g.setColor(Color.RED);
			g.drawString("PAUSED", 10, 60);
		}
		for (Obstacle obstacle : obstacles) {
			if (obstacle == currentObstacle) {
				g.setColor(Color.RED);
			} else {
				g.setColor(Color.BLACK);
			}
			g.drawPolygon(obstacle);
		}
		if (mouseLocation != null) {
			
			g.drawOval(mouseLocation.x - 5, mouseLocation.y - 5, 10, 10);
		}
		if (thingLocation != null) {
			// todo: how can we get this to leave a trail?
			// we clearly don't want to remember the whole path, right?
			// or perhaps we do!
			g.fillRect(thingLocation.x, thingLocation.y, 5, 5);
		}
		
		if (mouseLocation != null && thingLocation != null) {
			g.setColor(Color.RED);
			g.drawLine(thingLocation.x, thingLocation.y, mouseLocation.x, mouseLocation.y);
			if (movementAimTarget != null) {
				g.setColor(Color.GREEN);
				g.drawLine(thingLocation.x, thingLocation.y, movementAimTarget.x, movementAimTarget.y);
			}
		}

		g.setColor(Color.BLACK);
		int i = 1;
		if (currentWaypoint != null) {
			g.drawString(String.valueOf(i), currentWaypoint.x, currentWaypoint.y);
		}
		for (Point waypoint : waypoints) {
			i++;
			g.drawString(String.valueOf(i), waypoint.x, waypoint.y);
		}
		
	}

	@Override
	public void run() {
		thingLocation = new Point(getWidth() / 2, getHeight() / 2);
		while (true) {
			try {
				Thread.sleep(Math.max(10, delay));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (!paused) {
				generateNavigationMesh();
				if (currentWaypoint == null) {
					currentWaypoint = waypoints.poll();
				}
				moveThing();
			}
		}
	}

	
	private void generateNavigationMesh() {
		/*
		A navigation mesh always has at least 4 points, the 4 corners of the board. 
		As new obstacles are added to the board, the mesh will lose or gain points
		depending on where the obstacles were places. Intuitively the mesh would
		only gain points, however it's conceivable that an obstacle is places in 
		such a way that other obstacles are joined, potentially decreasing the 
		number of points
		
		 */
		List<Point> mesh = new ArrayList<Point>();
		
		// start by adding the four corners of the board
		mesh.add(new Point(0, 0));
		mesh.add(new Point(0, getHeight()));
		mesh.add(new Point(getWidth(), 0));
		mesh.add(new Point(getWidth(), getHeight()));
		
		/*
		
		// now add all the obstacles, potentially merging obstacles to form polygons
		for (Obstacle obstacle : obstacles) {
			// this is a bit of a naïve approach, but it should work with a small number of obstacles
			for (Point corner : obstacle.getCorners()) {
				for (Obstacle innerObstacle : obstacles) {
					if (innerObstacle.contains(corner)) {
						// todo: merge these two based on this collision.
						// find out if there's a better way of doing this
						
					}
				}
			}
		}
		*/
		
	}

	/**
	 * Moves the thing closer to the mouse.
	 */
	private void moveThing() {
		Point target = mouseLocation;
		if (currentWaypoint != null) {
			target = currentWaypoint;
		}
		if (target != null && thingLocation != null) {
			Point expectedMovementTarget = moveTowards(thingLocation, target, speed);
			movementAimTarget = moveTowards(thingLocation, target, 30);

			currentObstacle = null;
			// avoid obstacle
			for (Obstacle obstacle : obstacles) {
				if (obstacle.contains(expectedMovementTarget)) {
					// find a way around it
					// should also highlight the obstacle it's currently working its way around
					currentObstacle = obstacle;
					
					Point bestCorner = findBestCorner(thingLocation, obstacle, target);
					expectedMovementTarget = moveTowards(thingLocation, bestCorner, speed);
					break; // todo: this will do for now, but if we expect to be able to make it out of real labyrinths, we're going to have to do some real path-finding 
				}
			}

			thingLocation = expectedMovementTarget;
			
			// todo: when we get close enough to the waypoint (or the target), we must set the waypoint to null

			double distanceToTarget = distanceBetween(thingLocation, target);
			if (distanceToTarget < speed) {
				// we have arrived
				currentWaypoint = null;
				return;
			}



			repaint();
		}
		
	}

	private Point findBestCorner(final Point thingLocation, Obstacle obstacle, Point mouseLocation) {
		// find the two corners closest to the thing. These are the only potential corners.
		// if we checked all of them, we'd end up teleporting through the obstacle, as the closest
		// corner to the mouse location is a corner on the other side.
		// the exception is when the mouse location is on the SIDE of the obstacle, but then
		// this algorithm still works
		
		List<Point> cornersByProximityToThing = obstacle.getCorners();
		Collections.sort(cornersByProximityToThing, new Comparator<Point>() {
			@Override
			public int compare(Point o1, Point o2) {
				double distanceToFirst = distanceBetween(thingLocation, o1);
				double distanceToSecond = distanceBetween(thingLocation, o2);
				return Double.compare(distanceToFirst, distanceToSecond);
			}
		});

		double shortestDistance = Double.MAX_VALUE;
		Point bestCorner = null;
		// check the first two corners
		for (int i = 0; i < 2; i++) {
			Point corner = cornersByProximityToThing.get(i);
			double distanceFromCornerToMouseLocation = distanceBetween(corner, mouseLocation);
			if (distanceFromCornerToMouseLocation < shortestDistance) {
				shortestDistance = distanceFromCornerToMouseLocation;
				bestCorner = corner;
			}
		}
		return bestCorner;

	}
	
	private double distanceBetween(Point a, Point b) {
		double dx = a.x - b.x;
		double dxs = Math.pow(dx, 2);
		double dy = a.y - b.y;
		double dys = Math.pow(dy, 2);
		double sum = dxs + dys;
		return Math.sqrt(sum);
	}

	private Point moveTowards(Point source, Point target, int speed) {
		int opposite = target.y - source.y; // ordinate
		int adjacent = target.x - source.x; // abscissa
		double angle = Math.atan2(opposite, adjacent);

		int xMovement = (int) Math.round(Math.cos(angle) * speed);
		int yMovement = (int) Math.round(Math.sin(angle) * speed);
		
		// todo: if we use Point2D instead, with doubles or floats, we get smoother movement, and we'll need it anyway later when we start doing math
		
		
		return new Point(source.x + xMovement, source.y + yMovement);
	}
}
