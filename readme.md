# Pathfinder

This was supposed to be some kind of pathfinding algorithm, but I didn't get very far.
Currently it's only a dot that follows the cursor around. You can change the speed of the dot using the mouse wheel.

You can set waypoints via shift-click, and place obstacles via right-click.